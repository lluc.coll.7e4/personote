package com.itb.personote.views.menus

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itb.personote.adapters.DocumentAdapter
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel

class SearchFragment: Fragment(R.layout.search_fragment), DocumentAdapter.OnItemClickListener {
    lateinit var userIcon: ImageView
    lateinit var searchBar: SearchView
    private lateinit var recyclerView: RecyclerView

    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userIcon = view.findViewById(R.id.user_icon)
        searchBar = view.findViewById(R.id.searchBar)
        recyclerView = view.findViewById(R.id.documents_recycler_view)


        if(viewModel.hasImage) {
            userIcon.setImageBitmap(viewModel.userImage)
        }

        userIcon.setOnClickListener{
            val action = SearchFragmentDirections.actionSearchFragmentToModifyUser()
            findNavController().navigate(action)
        }

        recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        val docAdapter = DocumentAdapter(viewModel.getSearch(), this, viewModel.email)
        viewModel.MutableDocsList.observe(viewLifecycleOwner, {recyclerView.adapter = docAdapter})

        searchBar.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val docAdapter = DocumentAdapter(viewModel.search(newText!!), this@SearchFragment, viewModel.email)
                recyclerView.adapter = docAdapter
                return false
            }
        })

    }

    override fun onItemClick(id: Int, private: Boolean) {
        val action = SearchFragmentDirections.actionSearchFragmentToOnDocument(id, private)
        findNavController().navigate(action)
    }
}