package com.itb.personote.views.documentRelated

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import android.content.DialogInterface.OnMultiChoiceClickListener
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.itb.personote.models.Document
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel

class OnDocumentEdit: Fragment(R.layout.on_document_edit) {
    lateinit var goBack: ImageView
    lateinit var fav: ImageView
    lateinit var del: ImageView
    lateinit var makePublic: Button
    lateinit var name: TextInputEditText
    lateinit var desc: TextInputEditText
    lateinit var categories: ImageView
    lateinit var categoriesT: TextView
    lateinit var upload: Button

    private val db = FirebaseFirestore.getInstance()
    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goBack = view.findViewById(R.id.goBack)
        fav = view.findViewById(R.id.fav)
        del = view.findViewById(R.id.del)
        makePublic = view.findViewById(R.id.makePublic)
        name = view.findViewById(R.id.name)
        categories = view.findViewById(R.id.categories)
        categoriesT = view.findViewById(R.id.categoriesT)
        desc = view.findViewById(R.id.description)
        upload = view.findViewById(R.id.upLoad)


        val pos = arguments?.getInt("pos")

        val doc = viewModel.DocsList[pos!!]

        name.setText(doc.name)
        desc.setText(doc.description)

        categoriesT.text = doc.categories.toString()

        if (viewModel.favourites.contains(doc.id)){
            fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
        }else{
            fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.base2), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        if(doc.public){
            makePublic.setText("Make Private")
        }else{
            makePublic.setText("Make Public")
        }

        makePublic.setOnClickListener{
            doc.public = !doc.public
            if(doc.public){
                makePublic.setText("Make Private")
            }else{
                makePublic.setText("Make Public")
            }
        }

        val mUserItems: ArrayList<Int> = ArrayList()
        for (i in 0..doc.checkedItems!!.size-1){
            if (doc.checkedItems!![i]){
                mUserItems.add(i)
            }
        }

        categories.setOnClickListener{
            var catAlert = AlertDialog.Builder(context)
            catAlert.setTitle("Categories")
            catAlert.setCancelable(false)
            catAlert.setMultiChoiceItems(viewModel.listCategories.toTypedArray(), doc.checkedItems,
                OnMultiChoiceClickListener { dialogInterface, position, isChecked ->
//                                           if (isChecked) {
//                                                if (!mUserItems.contains(position)) {
//                                                    mUserItems.add(position);
//                                                }
//                                            } else if (mUserItems.contains(position)) {
//                                                mUserItems.remove(position);
//                                            }
                    if (isChecked) {
                        mUserItems.add(position)
                    } else {
                        mUserItems.remove(Integer.valueOf(position))
                    }
                })
            catAlert.setPositiveButton("Ok",
                DialogInterface.OnClickListener { dialogInterface, which ->
                    var item = ""
                    for (i in 0 until mUserItems.size) {
                        item = item + viewModel.listCategories[mUserItems[i]]
                        if (i != mUserItems.size - 1) {
                            item = "$item, "
                        }
                    }
                    categoriesT.setText("["+item+"]")

                })

            catAlert.setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })

            catAlert.show()

        }

        fav.setOnClickListener {
            if (viewModel.favourites.contains(doc.id)){
                viewModel.favourites.removeAt(viewModel.favourites.indexOf(doc.id))
                fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.base2), android.graphics.PorterDuff.Mode.MULTIPLY);

                db.collection("users/${viewModel.email}/favs").whereEqualTo("document", doc.id).get().addOnSuccessListener { result ->
                    for (document in result) {
                        val id = document.id
                        db.collection("users/${viewModel.email}/favs").document(id).delete()
                    }
                }
            }else{
                viewModel.favourites.add(doc.id)
                fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);

                db.collection("users/${viewModel.email}/favs").document().set(
                    hashMapOf(
                        "document" to doc.id,
                    )
                )
            }
        }

        goBack.setOnClickListener{
            val action = OnDocumentEditDirections.actionOnDocumentEditToOnDocument(pos, true)
            findNavController().navigate(action)
        }

        upload.setOnClickListener{
            pujarFirebase(doc)

            Toast.makeText(requireContext(), "Changes uploaded!", Toast.LENGTH_SHORT).show()
            val action = OnDocumentEditDirections.actionOnDocumentEditToOnDocument(pos, true)
            findNavController().navigate(action)
        }

        del.setOnClickListener{
            val dialog = AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage("Do you want to delete this document?")
                .setNegativeButton("Cancel") { view, _ -> }
                .setPositiveButton("Continue") { view, _ ->
                    db.collection("documents").document(doc.id).delete()
                    FirebaseStorage.getInstance().reference.child("documents/${doc.id}").delete()
                    viewModel.getHomeDocs()
                    val action = OnDocumentEditDirections.actionOnDocumentEditToHomeFragment()
                    findNavController().navigate(action)
                }
                .setCancelable(false)
                .create()

            dialog.show()
        }


    }

    private fun pujarFirebase(doc: Document){
        var public = "PRIVATE"
        if (doc.public){
            public = "PUBLIC"
        }
        db.collection("documents").document(doc.id).set(
            hashMapOf(
                "nom" to name.text.toString(),
                "descripcio" to desc.text.toString(),
                "propietari" to viewModel.email,
                "public" to public
            )
        )



        for (i in 0..doc.checkedItems!!.size-1){
            if (doc.checkedItems!![i] && !doc.categories!!.contains(viewModel.listCategories!![i])){
                db.collection("documents/${doc.id}/categories").document().set(
                    hashMapOf(
                        "nom" to viewModel.listCategories!![i],
                    )
                )
            }

            else if(!doc.checkedItems!![i]){
                db.collection("documents/${doc.id}/categories").whereEqualTo("nom", viewModel.listCategories!![i]).get().addOnSuccessListener { result ->
                    for (document in result) {
                        val id = document.id
                        db.collection("documents/${doc.id}/categories").document(id).delete()
                    }
                }
            }
        }

        doc.categories!!.removeAll(doc.categories!!)
        for (i in 0..doc.checkedItems!!.size-1){
            if(doc.checkedItems!![i]){
                doc.categories!!.add(viewModel.listCategories!![i])
            }
        }
    }
}