package com.itb.personote.views.userRelated

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.itb.personote.R

class RegisterFragment: Fragment(R.layout.register_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var register: TextView
    lateinit var next: Button


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.slide_left)
        enterTransition = inflater.inflateTransition(R.transition.slide_right)


        username = view.findViewById(R.id.name)
        password = view.findViewById(R.id.age)
        register = view.findViewById(R.id.register)
        next = view.findViewById(R.id.done)

        register.setOnClickListener{
            Navigation.findNavController(view).navigate(R.id.loginFragment)
        }

        next.setOnClickListener {
            val action = RegisterFragmentDirections.actionRegisterFragmentToRegister2Fragment(username.text.toString(), password.text.toString())
            findNavController().navigate(action)
        }
    }
}