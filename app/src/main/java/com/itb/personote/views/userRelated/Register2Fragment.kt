package com.itb.personote.views.userRelated

import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel

class Register2Fragment: Fragment(R.layout.modify_user) {
    lateinit var userImage: ImageView
    lateinit var name: TextInputEditText
    lateinit var surname: TextInputEditText
    lateinit var age: TextInputEditText
    lateinit var tel: TextInputEditText
    lateinit var city: TextInputEditText
    lateinit var insti: TextInputEditText
    lateinit var goBack: ImageView
    lateinit var done: Button
    lateinit var logout: Button


    private val db = FirebaseFirestore.getInstance()


    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.slide_left)
        enterTransition = inflater.inflateTransition(R.transition.slide_right)

        userImage = view.findViewById(R.id.userImage)
        name = view.findViewById(R.id.name)
        surname = view.findViewById(R.id.surname)
        age = view.findViewById(R.id.age)
        tel = view.findViewById(R.id.tel)
        city = view.findViewById(R.id.city)
        insti = view.findViewById(R.id.insti)
        goBack = view.findViewById(R.id.goBack)
        done = view.findViewById(R.id.done)
        logout = view.findViewById(R.id.logout)

        logout.visibility = View.INVISIBLE

        val email = arguments?.getString("email")
        val password = arguments?.getString("password")

        goBack.setOnClickListener{
            val action = Register2FragmentDirections.actionRegister2FragmentToRegisterFragment()
            findNavController().navigate(action)
        }

        done.setOnClickListener{
            if(name.text!!.length != 0 && surname.text!!.length != 0 && age.text!!.length != 0 && tel.text!!.length != 0 && city.text!!.length != 0 && insti.text!!.length != 0) {
                FirebaseAuth.getInstance().
                createUserWithEmailAndPassword(email!!, password!!)
                    .addOnCompleteListener {
                        if(it.isSuccessful){
                            val emailLogged = it.result?.user?.email
                            viewModel.email = emailLogged.toString()
                            db.collection("users").document(email).set(
                                hashMapOf(
                                    "nom" to name.text.toString(),
                                    "email" to viewModel.email,
                                    "cognoms" to surname.text.toString(),
                                    "edat" to age.text.toString(),
                                    "tel" to tel.text.toString(),
                                    "localitat" to city.text.toString(),
                                    "institut" to insti.text.toString()
                                )
                            )
                            viewModel.getUser(emailLogged.toString(), true)
                            val action = Register2FragmentDirections.actionRegister2FragmentToHomeFragment()
                            findNavController().navigate(action)
                        }
                        else{
                            MaterialAlertDialogBuilder(requireContext())
                                .setTitle("Error")
                                .setMessage(resources.getString(R.string.registerError))
                                .setNegativeButton("Accept") { dialog, which ->
                                    val action = Register2FragmentDirections.actionRegister2FragmentToRegisterFragment()
                                    findNavController().navigate(action)
                                }
                                .show()
                        }
                    }
            }
            else{
                Toast.makeText(requireContext(), "Complete all fields!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}