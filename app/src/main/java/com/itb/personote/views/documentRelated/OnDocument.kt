package com.itb.personote.views.documentRelated

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel
import java.io.File

class OnDocument : Fragment(R.layout.on_document) {
    lateinit var goBack: ImageView
    lateinit var fav: ImageView
    lateinit var edit: ImageView
    lateinit var name: TextView
    lateinit var desc: TextView
    lateinit var categories: TextView
    lateinit var download: Button

    private val db = FirebaseFirestore.getInstance()
    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        goBack = view.findViewById(R.id.goBack)
        fav = view.findViewById(R.id.fav)
        edit = view.findViewById(R.id.edit)
        name = view.findViewById(R.id.name)
        desc = view.findViewById(R.id.description)
        categories = view.findViewById(R.id.categories)
        download = view.findViewById(R.id.download)


        val pos = arguments?.getInt("pos")
        val private = arguments?.getBoolean("private")

        val doc = viewModel.DocsList[pos!!]

        categories.text = doc.categories.toString()

        if (viewModel.favourites.contains(doc.id)){
            fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
        }else{
            fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.base2), android.graphics.PorterDuff.Mode.MULTIPLY);
        }

        name.setText(doc.name)
        desc.setText(doc.description)

        edit.setOnClickListener{
            if(private!!){
                val action = OnDocumentDirections.actionOnDocumentToOnDocumentEdit(pos)
                findNavController().navigate(action)
            }
            else {
                Toast.makeText(requireContext(), "You are not the owner of this document!", Toast.LENGTH_LONG).show()
            }
        }

        goBack.setOnClickListener{
            val action = OnDocumentDirections.actionOnDocumentToHomeFragment()
            findNavController().navigate(action)
        }

        fav.setOnClickListener {
            if (viewModel.favourites.contains(doc.id)){
                viewModel.favourites.removeAt(viewModel.favourites.indexOf(doc.id))
                fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.base2), android.graphics.PorterDuff.Mode.MULTIPLY);

                db.collection("users/${viewModel.email}/favs").whereEqualTo("document", doc.id).get().addOnSuccessListener { result ->
                    for (document in result) {
                        val id = document.id
                        db.collection("users/${viewModel.email}/favs").document(id).delete()
                    }
                }
            }else{
                viewModel.favourites.add(doc.id)
                fav.setColorFilter(ContextCompat.getColor(requireContext(), R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);

                db.collection("users/${viewModel.email}/favs").document().set(
                    hashMapOf(
                        "document" to doc.id,
                    )
                )
            }
        }

        download.setOnClickListener{
            val storage = FirebaseStorage.getInstance().reference.child("documents/${doc.id}")
            val localFile = File.createTempFile("document", "pdf", File("/storage/emulated/0/Download"))
            storage.getFile(localFile).addOnSuccessListener {
                Toast.makeText(requireContext(), "File downloaded at ${localFile.absolutePath}!", Toast.LENGTH_LONG).show()
            }.addOnFailureListener {
                Toast.makeText(requireContext(), "Error downloading file!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}