package com.itb.personote.views.documentRelated

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import android.content.DialogInterface
import android.content.DialogInterface.OnMultiChoiceClickListener
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel


class UploadDocument : Fragment(R.layout.add_document) {
    lateinit var goBack: ImageView
    lateinit var upLoadFile: ImageView
    lateinit var fileName: TextInputEditText
    lateinit var description: TextInputEditText
    lateinit var upload: Button
    lateinit var makePublic: Button
    lateinit var categories: ImageView
    lateinit var categoriesT: TextView
    lateinit var docSel: TextView
    lateinit var file: Uri

    var public = false
    var fileSelected = false
    private val db = FirebaseFirestore.getInstance()


    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Log.d("meh", "$result")
            if (result.resultCode == RESULT_OK) {
                val data: Intent? = result.data
                if (data != null) {
                    file = data.data!!
                    Log.d("meh", "${file.path}")
                    Toast.makeText(requireContext(), "File added!", Toast.LENGTH_SHORT).show()
                    fileSelected = true
                    docSel.text = file.lastPathSegment
                }
            }
        }


    private val viewModel: viewModel by activityViewModels()



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        goBack = view.findViewById(R.id.goBack)
        upLoadFile = view.findViewById(R.id.upLoadFile)
        fileName = view.findViewById(R.id.name)
        makePublic = view.findViewById(R.id.makePublic)
        description = view.findViewById(R.id.description)
        upload = view.findViewById(R.id.upLoad)
        categories = view.findViewById(R.id.categories)
        categoriesT = view.findViewById(R.id.categoriesT)
        docSel = view.findViewById(R.id.documentSelected)


        val mUserItems: ArrayList<Int> = ArrayList()
        var checkedItems = BooleanArray(viewModel.listCategories!!.size)

        if(public){
            makePublic.setText("Make Private")
        }else{
            makePublic.setText("Make Public")
        }

        makePublic.setOnClickListener{
            public = !public
            if(public){
                makePublic.setText("Make Private")
            }else{
                makePublic.setText("Make Public")
            }
        }

        categories.setOnClickListener{
            var catAlert = AlertDialog.Builder(context)
            catAlert.setTitle("Categories")
            catAlert.setCancelable(false)
            catAlert.setMultiChoiceItems(viewModel.listCategories.toTypedArray(), checkedItems,
               OnMultiChoiceClickListener { dialogInterface, position, isChecked ->
//                                           if (isChecked) {
//                                                if (!mUserItems.contains(position)) {
//                                                    mUserItems.add(position);
//                                                }
//                                            } else if (mUserItems.contains(position)) {
//                                                mUserItems.remove(position);
//                                            }
                    if (isChecked) {
                        mUserItems.add(position)
                    } else {
                        mUserItems.remove(Integer.valueOf(position))
                    }
                })
            catAlert.setPositiveButton("Ok",
                DialogInterface.OnClickListener { dialogInterface, which ->
                    var item = ""
                    for (i in 0 until mUserItems.size) {
                        item = item + viewModel.listCategories!![mUserItems[i]]
                        if (i != mUserItems.size - 1) {
                            item = "$item, "
                        }
                    }
                    categoriesT.setText(item)
                })

            catAlert.setNegativeButton("Cancel",
                DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })

            catAlert.show()
        }

        upLoadFile.setOnClickListener {
            selectImage()
        }

        upload.setOnClickListener {
            pujarFirebase(checkedItems)
        }

        goBack.setOnClickListener{
            val action = UploadDocumentDirections.actionUploadDocumentToHomeFragment()
            findNavController().navigate(action)
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "*/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }


    private fun pujarFirebase(checkedItems: BooleanArray) {
        var id: String? = null

        var publicT = "PRIVATE"
        if (public){
            publicT = "PUBLIC"
        }

        if(fileName.text!!.length != 0 && description.text!!.length != 0 && fileSelected) {
            db.collection("documents").document().set(
                hashMapOf(
                    "nom" to fileName.text.toString().toLowerCase(),
                    "descripcio" to description.text.toString(),
                    "propietari" to viewModel.email,
                    "public" to publicT
                )
            )

            db.collection("documents").whereEqualTo("nom", fileName.text.toString()).whereEqualTo("descripcio", description.text.toString()).get().addOnSuccessListener { result ->
                for (document in result) {
                    id = document.id
                }

                Log.d("encara", "${checkedItems.size}")

                for (i in 0..checkedItems!!.size-1){
                    if (checkedItems!![i]){
                        db.collection("documents/${id}/categories").document().set(
                            hashMapOf(
                                "nom" to viewModel.listCategories!![i],
                            )
                        )
                    }
                }

                val storage = FirebaseStorage.getInstance().getReference("documents/${id}")
                storage.putFile(file)
                    .addOnSuccessListener {
                        Toast.makeText(requireContext(), "File uploaded!", Toast.LENGTH_SHORT).show()

                        val action = UploadDocumentDirections.actionUploadDocumentToHomeFragment()
                        findNavController().navigate(action)
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "File not uploaded!", Toast.LENGTH_SHORT).show()
                    }
            }
        }
        else if (!fileSelected){
            Toast.makeText(requireContext(), "Select file!", Toast.LENGTH_SHORT).show()
        }
        else{
            Toast.makeText(requireContext(), "Complete all fields!", Toast.LENGTH_SHORT).show()
        }
    }
}