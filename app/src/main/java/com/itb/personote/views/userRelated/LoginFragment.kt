package com.itb.personote.views.userRelated

import android.os.Bundle
import android.transition.TransitionInflater
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel

class LoginFragment: Fragment(R.layout.login_fragment) {
    lateinit var username: TextInputEditText
    lateinit var password: TextInputEditText
    lateinit var register: TextView
    lateinit var login: Button

    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        exitTransition = inflater.inflateTransition(R.transition.slide_left)
        enterTransition = inflater.inflateTransition(R.transition.slide_right)


        username = view.findViewById(R.id.name)
        password = view.findViewById(R.id.age)
        register = view.findViewById(R.id.register)
        login = view.findViewById(R.id.done)

        //treure!!


        register.setOnClickListener{
            val action = LoginFragmentDirections.actionLoginFragmentToRegisterFragment()
            findNavController().navigate(action)
        }

        login.setOnClickListener{
            FirebaseAuth.getInstance().signOut()
            FirebaseAuth.getInstance().
            signInWithEmailAndPassword(username.text.toString(), password.text.toString())
                .addOnCompleteListener {
                    if(it.isSuccessful){
                        val emailLogged = it.result?.user?.email
                        viewModel.email = username.text.toString()
                        viewModel.getUser(username.text.toString(), true)
                        val action = LoginFragmentDirections.actionLoginFragmentToHomeFragment()
                        findNavController().navigate(action)
                    }
                    else{
                        password.error = "Error al login"
                        password.setText("")
                        Log.d("meh", "vaja")

                    }
                }
        }
    }
}