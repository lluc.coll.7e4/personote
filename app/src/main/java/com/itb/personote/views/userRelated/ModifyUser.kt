package com.itb.personote.views.userRelated

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel
import java.io.ByteArrayOutputStream

class
ModifyUser: Fragment(R.layout.modify_user) {
    lateinit var userImage: ImageView
    lateinit var name: TextInputEditText
    lateinit var surname: TextInputEditText
    lateinit var age: TextInputEditText
    lateinit var tel: TextInputEditText
    lateinit var city: TextInputEditText
    lateinit var insti: TextInputEditText
    lateinit var goBack: ImageView
    lateinit var done: Button
    lateinit var logout: Button

    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            result ->
        if (result.resultCode == RESULT_OK) {
            val data: Intent? = result.data
            if (data != null) {
                val imageUri = data.data!!
                val bitmap: Bitmap = MediaStore.Images.Media.getBitmap(requireContext().getContentResolver(), imageUri)
                viewModel.userImage = bitmap

                userImage.setImageBitmap(viewModel.userImage)
            }
        }
    }


    private val db = FirebaseFirestore.getInstance()

    private val viewModel: viewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userImage = view.findViewById(R.id.userImage)
        name = view.findViewById(R.id.name)
        surname = view.findViewById(R.id.surname)
        age = view.findViewById(R.id.age)
        tel = view.findViewById(R.id.tel)
        city = view.findViewById(R.id.city)
        insti = view.findViewById(R.id.insti)
        goBack = view.findViewById(R.id.goBack)
        done = view.findViewById(R.id.done)
        logout = view.findViewById(R.id.logout)

        viewModel.getUser(viewModel.email, true)

        if(viewModel.hasImage){
            userImage.setImageBitmap(viewModel.userImage)
        }

        userImage.setOnClickListener{
            selectImage()
        }

        logout.setOnClickListener{
            val email = viewModel.email
            val dialog = AlertDialog.Builder(context)
                .setTitle("Warning")
                .setMessage("Are you sure you want to logout from: "+email)
                .setNegativeButton("Cancel") { view, _ -> }
                .setPositiveButton("Continue") { view, _ ->
                    FirebaseAuth.getInstance().signOut()
                    viewModel.email = ""
                    viewModel.user = null
                    viewModel.userImage = null
                    viewModel.hasImage = false
                    val action = ModifyUserDirections.actionModifyUserToLoginFragment()
                    findNavController().navigate(action)
                }
                .setCancelable(false)
                .create()

            dialog.show()

        }

        goBack.setOnClickListener{
            viewModel.getUserImage(viewModel.email, true)
            val action = ModifyUserDirections.actionModifyUserToHomeFragment()
            findNavController().navigate(action)
        }


        name.setText(viewModel.user!!.name!!)
        surname.setText(viewModel.user!!.surname!!)
        age.setText(viewModel.user!!.age!!)
        tel.setText(viewModel.user!!.phone!!)
        city.setText(viewModel.user!!.city!!)
        insti.setText(viewModel.user!!.insti!!)


        done.setOnClickListener{
            if(name.text!!.length != 0 && surname.text!!.length != 0 && age.text!!.length != 0 && tel.text!!.length != 0 && city.text!!.length != 0 && insti.text!!.length != 0) {
                db.collection("users").document(viewModel.email).set(
                    hashMapOf(
                        "nom" to name.text.toString(),
                        "email" to viewModel.email,
                        "cognoms" to surname.text.toString(),
                        "edat" to age.text.toString(),
                        "tel" to tel.text.toString(),
                        "localitat" to city.text.toString(),
                        "institut" to insti.text.toString()
                    )
                )

                val storage = FirebaseStorage.getInstance().getReference("fotosPerfil/${viewModel.email}")

                val baos = ByteArrayOutputStream()
                viewModel.userImage!! .compress(Bitmap.CompressFormat.JPEG, 100, baos)
                val data = baos.toByteArray()
                storage.putBytes(data)
                    .addOnSuccessListener {
                        Toast.makeText(requireContext(), "Image uploaded!", Toast.LENGTH_SHORT).show()
                        val action = ModifyUserDirections.actionModifyUserToHomeFragment()
                        findNavController().navigate(action)
                    }
                    .addOnFailureListener {
                        Toast.makeText(requireContext(), "Image not uploaded!", Toast.LENGTH_SHORT).show()
                    }
            }
            else{
                Toast.makeText(requireContext(), "Complete all fields!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        resultLauncher.launch(intent)
    }
}