package com.itb.personote.views.menus

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import android.widget.ImageView
import android.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.itb.personote.adapters.DocumentAdapter
import com.itb.personote.R
import com.itb.personote.viewModels.viewModel

class HomeFragment: Fragment(R.layout.home_fragment), DocumentAdapter.OnItemClickListener {
    lateinit var userIcon: ImageView
    lateinit var searchBar: SearchView
    private lateinit var recyclerView: RecyclerView
    lateinit var plusIcon: FloatingActionButton

    private val viewModel: viewModel by activityViewModels()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userIcon = view.findViewById(R.id.user_icon)
        searchBar = view.findViewById(R.id.searchBar)
        recyclerView = view.findViewById(R.id.recycler_view)
        plusIcon = view.findViewById(R.id.button)



        if(viewModel.hasImage) {
            userIcon.setImageBitmap(viewModel.userImage)
        }

        userIcon.setOnClickListener{
            val action = HomeFragmentDirections.actionHomeFragmentToModifyUser()
            findNavController().navigate(action)
        }

        plusIcon.setOnClickListener{
            val action = HomeFragmentDirections.actionHomeFragmentToUploadDocument()
            findNavController().navigate(action)
        }

        viewModel.getHomeDocs()
        recyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        val docAdapter = DocumentAdapter(viewModel.DocsList, this, viewModel.email)
        viewModel.MutableDocsList.observe(viewLifecycleOwner, {recyclerView.adapter = docAdapter})

        searchBar.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val docAdapter = DocumentAdapter(viewModel.search(newText!!), this@HomeFragment, viewModel.email)
                recyclerView.adapter = docAdapter
                return false
            }
        })

    }

    override fun onItemClick(id: Int, private: Boolean) {
        val action = HomeFragmentDirections.actionHomeFragmentToOnDocument(id, private)
        findNavController().navigate(action)
    }
}