package com.itb.personote.viewModels

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.itb.personote.models.Document
import com.itb.personote.models.User
import java.io.File

class viewModel: ViewModel() {
    var email = ""
    var user : User? = null
    var userImage : Bitmap? = null

    var listCategories = mutableListOf<String>()

    var hasImage = false
    var tempUser: User? = null
    var tempImage: Bitmap? = null

    private val db = FirebaseFirestore.getInstance()
    var DocsList = mutableListOf<Document>()
    var MutableDocsList = MutableLiveData<List<Document>>()
    var favourites = mutableListOf<String>()


    init {
        getCategories()
    }

    fun getHomeDocs(): List<Document>{
        DocsList.removeAll(DocsList)
        var meh = mutableListOf<String>()

        db.collection("documents").whereEqualTo("propietari", email).get().addOnSuccessListener {result ->
            for (document in result) {
                val id = document.id
                val name = document.get("nom")as String
                val desc = document.get("descripcio")as String
                val public = document.get("public")as String
                val boss = document.get("propietari")as String

                meh.add(id)
                var publ = false
                if (public.equals("PUBLIC")){publ = true}

                var cats = mutableListOf<String>()

                db.collection("documents/${id}/categories").get().addOnSuccessListener { result1 ->
                    for (doc in result1) {
                        val nom = doc.get("nom") as String
                        cats.add(nom)
                    }
                    var checked = BooleanArray(listCategories!!.size)
                    for (i in 0..listCategories!!.size-1){
                        for (j in 0..cats.size-1){
                            if(listCategories!![i] == cats[j]){
                                checked[i] = true
                            }
                        }
                    }
                    DocsList.add(Document(id, name, desc, publ, boss, cats!!, checked))
                    MutableDocsList.postValue(DocsList)
                }
            }
        }
        return DocsList
    }


    fun getSearch(): List<Document>{
        DocsList.removeAll(DocsList)
        db.collection("documents").whereEqualTo("public", "PUBLIC").get().addOnSuccessListener {result ->
            for (document in result) {
                val id = document.id
                val name = document.get("nom")as String
                val desc = document.get("descripcio")as String
                val public = document.get("public")as String
                val boss = document.get("propietari")as String

                var publ = false
                if (public.equals("PUBLIC")){publ = true}


                var cats = mutableListOf<String>()

                db.collection("documents/${id}/categories").get().addOnSuccessListener { result1 ->
                    for (doc in result1) {
                        val nom = doc.get("nom") as String
                        cats.add(nom)
                    }
                    var checked = BooleanArray(listCategories!!.size)
                    for (i in 0..listCategories!!.size-1){
                        for (j in 0..cats.size-1){
                            if(listCategories!![i] == cats[j]){
                                checked[i] = true
                            }
                        }
                    }
                    DocsList.add(Document(id, name, desc, publ, boss, cats!!, checked))
                    MutableDocsList.postValue(DocsList)
                }
            }
        }
        return DocsList
    }

    fun getFavs(first: Boolean): List<Document>{

        DocsList.removeAll(DocsList)
        favourites.removeAll(favourites)
        db.collection("users/${email}/favs").get().addOnSuccessListener { result ->
            var id: String? = null
            for (i in result) {
                id = i.get("document") as String

                favourites.add(id)
            }

            if(!first){

            for (f in favourites) {
                db.collection("documents/").document("/${f}").get().addOnSuccessListener { i ->
                    val id = i.id
                    val name = i.get("nom") as String
                    val desc = i.get("descripcio") as String
                    val public = i.get("public") as String
                    val boss = i.get("propietari") as String

                    var publ = false
                    if (public.equals("PUBLIC")) {
                        publ = true
                    }


                    var cats = mutableListOf<String>()

                    db.collection("documents/${id}/categories").get()
                        .addOnSuccessListener { result1 ->
                            for (doc in result1) {
                                val nom = doc.get("nom") as String
                                cats.add(nom)
                            }
                            var checked = BooleanArray(listCategories!!.size)
                            for (i in 0..listCategories!!.size - 1) {
                                for (j in 0..cats.size - 1) {
                                    if (listCategories!![i] == cats[j]) {
                                        checked[i] = true
                                    }
                                }
                            }
                            DocsList.add(Document(id, name, desc, publ, boss, cats!!, checked))
                            MutableDocsList.postValue(DocsList)
                        }
                }
            }
            }
        }

        return DocsList
    }


    fun getUserImage(gmail: String, boo: Boolean){
        val storage = FirebaseStorage.getInstance().reference.child("fotosPerfil/${gmail}")
        val localFile = File.createTempFile("temp", "jpeg")
        storage.getFile(localFile).addOnSuccessListener {
            tempImage = BitmapFactory.decodeFile(localFile.absolutePath)
            if (boo){
                userImage = tempImage
                tempUser!!.image = tempImage
                user = tempUser
                hasImage = true
                Log.d("meh", "$user")
            }
        }.addOnFailureListener{
            hasImage = false
            if (boo){
                user = tempUser
                Log.d("aiai", "$user")
            }
        }
        localFile.delete()
    }

    fun getUser(gmail: String, boo: Boolean) {
        var name: String? = null
        var surname: String? = null
        var age: String? = null
        var phone: String? = null
        var city: String? = null
        var insti: String? = null
        db.collection("users/").document("/${gmail}").get().addOnSuccessListener { i ->
            name = i.get("nom")as String
            surname = i.get("cognoms")as String
            age = i.get("edat")as String
            phone = i.get("tel")as String
            city = i.get("localitat")as String
            insti = i.get("institut")as String

            tempUser = User(gmail, name!!, surname!!, age!!, phone!!, city!!, insti!!)
            Log.d("ai", "$tempUser")

            getUserImage(gmail, boo)
        }
        getFavs(true)
    }

    fun getCategories(){
        db.collection("categories").get().addOnSuccessListener { result ->
            for (document in result) {
                val nom = document.get("nom")as String
                listCategories.add(nom)
            }
        }
    }


    fun search(text: String): MutableList<Document>{
        var docus = mutableListOf<Document>()

        for (i in DocsList){
            if(i.name.contains(text, ignoreCase = true)){
                docus.add(i)
            }
        }

        return docus
    }
}