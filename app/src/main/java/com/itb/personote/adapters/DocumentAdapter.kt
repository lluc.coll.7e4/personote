package com.itb.personote.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.itb.personote.R
import com.itb.personote.models.Document


class DocumentAdapter(private val docs: List<Document>, private val listener: OnItemClickListener, private val email: String) : RecyclerView.Adapter<DocumentAdapter.ListOfChampsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListOfChampsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.document,parent,false)

        return ListOfChampsViewHolder(view)
    }

    override fun onBindViewHolder(holderOfLists: ListOfChampsViewHolder, position: Int) {
        holderOfLists.bindData(docs[position])
    }

    override fun getItemCount(): Int {
        return docs.size
    }

    inner class ListOfChampsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var documentName: TextView


        init {
            documentName = itemView.findViewById(R.id.doc_name)
        }

        fun bindData(doc: Document){
            documentName.text = doc.name

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                if(docs[position].boss == email){
                    listener.onItemClick(position, true)
                }
                else{
                    listener.onItemClick(position, false)
                }
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(id: Int, private: Boolean)
    }
}
