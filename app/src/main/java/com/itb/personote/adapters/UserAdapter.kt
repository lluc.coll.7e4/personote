package com.itb.personote.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.itb.personote.R
import com.itb.personote.models.User


class UserAdapter(private val users: List<User>, private val listener: OnItemClickListener) : RecyclerView.Adapter<UserAdapter.ListOfChampsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListOfChampsViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user,parent,false)

        return ListOfChampsViewHolder(view)
    }

    override fun onBindViewHolder(holderOfLists: ListOfChampsViewHolder, position: Int) {
        holderOfLists.bindData(users[position])
    }

    override fun getItemCount(): Int {
        return users.size
    }

    inner class ListOfChampsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private var UserImage: ImageView
        private var UserName: TextView

        init {
            UserImage = itemView.findViewById(R.id.user_icon)
            UserName = itemView.findViewById(R.id.user_name)

        }

        fun bindData(user: User){
            UserName.text = user.name

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onItemClick(1)
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClick(position: Int)
    }
}
