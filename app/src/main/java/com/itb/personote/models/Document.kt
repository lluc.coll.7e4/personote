package com.itb.personote.models

data class Document (var id: String, var name: String, var description: String, var public: Boolean, var boss: String, var categories: MutableList<String>? = null, var checkedItems: BooleanArray? = null)