package com.itb.personote.models

import android.graphics.Bitmap

data class User (var email: String?, var name: String?, var surname: String?, var age: String?, var phone: String?, var city: String?, var insti: String?, var image: Bitmap? = null)